import pygame, math, time, random
from copy import deepcopy as dc

SCREEN_SIZE = [800,500]

class AsteroidsObject():
  shape = [[-1,-1],[-1,1],[1,1],[1,-1]]
  def __init__(self, screen_size: list[int], pos: list[int]=[0,0],rot: float=math.radians(90), scale: float=10, velocity: list[float]=[0,0]):
    self.pos = pos
    self.rot = rot
    self.scale = scale
    self.velocity = velocity
    self.screen_size = screen_size

    self.oldpos = [0,0]
    self.update()
  
  def draw(self, surface: pygame.Surface, color: tuple[int]=(255,255,255)) -> None:
    pygame.draw.polygon(surface, color, [[x*self.scale+self.pos[0], y*self.scale+self.pos[1]] for x,y in self.get_shape(self.rot)])

  def get_shape(self, rot: float=0):
    # rotate
    # https://stackoverflow.com/questions/45357715/how-to-rotate-point-around-another-one
    xAxis = [1,0]
    yAxis = [0,1]

    xAxis[0] = math.cos(rot)
    xAxis[1] = math.sin(rot)
    yAxis[0] = math.cos(rot + math.radians(90))
    yAxis[1] = math.sin(rot + math.radians(90))

    rotated_shape = []
    for point in self.shape:
      p = [0,0]
      p[0] = point[0] * xAxis[0]
      p[1] = point[0] * xAxis[1]

      p[0] = p[0] + point[1] * yAxis[0]
      p[1] = p[1] + point[1] * yAxis[1]

      rotated_shape.append(p)

    return rotated_shape
  
  def update(self):
    if self.pos[0] > self.screen_size[0]+self.scale/2:
      self.pos[0] = -self.scale/2
    if self.pos[0] < -self.scale/2:
      self.pos[0] = self.screen_size[0]+self.scale/2
    if self.pos[1] > self.screen_size[1]+self.scale/2:
      self.pos[1] = -self.scale/2
    if self.pos[1] < -self.scale/2:
      self.pos[1] = self.screen_size[1]+self.scale/2
    
    self.oldpos[0] = dc(self.pos[0])
    self.oldpos[1] = dc(self.pos[1])

    self.pos[0] += self.velocity[0]
    self.pos[1] += self.velocity[1]


class Ship(AsteroidsObject):
  shape = [[0,0],[1,1],[0,-1],[-1,1]]
  def update(self):
    if self.velocity[0] < 0:
      self.velocity[0] += 0.01
    if self.velocity[0] > 0:
      self.velocity[0] -= 0.01
    if self.velocity[1] < 0:
      self.velocity[1] += 0.01
    if self.velocity[1] > 0:
      self.velocity[1] -= 0.01
    super().update()

class Bullet(AsteroidsObject):
  shape = [[0,0],[0,1],[0,0]]
  def __init__(self, screen_size: list[int], pos: list[int], rot: float, scale: float=3, init_velocity: list[float]=[0,0], velocity: float=10):
    velocity2 = [velocity*math.sin(rot)+init_velocity[0], -velocity*math.cos(rot)+init_velocity[1]]
    self.clock = 30
    super().__init__(screen_size, pos, rot, scale, velocity2)
  def update(self):
    self.clock -= 1
    super().update()

class Asteroid(AsteroidsObject):
  shape = [[-0.5,1],[0.5,1],[1,0.5],[1,-0.5],[0.5,-1],[-0.5,-1],[-1,-0.5],[-1,0.5]]
  def __init__(self, screen_size: list[int], pos: list[int]=[None,None], rot: float=None, scale: float=10, velocity: float=None):
    pos = dc(pos)
    if pos[0] == None:
      pos[0] = random.random()*screen_size[0]
    if pos[1] == None:
      pos[1] = random.random()*screen_size[1]
    if rot == None:
      rot = random.random()*math.radians(360)
    if velocity == None:
      velocity = random.random()*3

    self.velocity = [0,0]
    self.velocity[0] = velocity*math.sin(rot)
    self.velocity[1] = -velocity*math.cos(rot)

    self.pos = pos
    self.rot = rot
    self.scale = scale
    self.screen_size = screen_size
    self.oldpos = [0,0]

    self.update()

  def collides_with(self, asteroids_object: AsteroidsObject, size: float=0):
    posi = [0,0]
    posd = [0,0]
    posd[0] = (asteroids_object.pos[0]-asteroids_object.oldpos[0])/30
    posd[1] = (asteroids_object.pos[1]-asteroids_object.oldpos[1])/30
    for i in range(30):
      posi[0] = asteroids_object.oldpos[0]+posd[0]*i
      posi[1] = asteroids_object.oldpos[1]+posd[1]*i
      if math.sqrt((self.pos[0]-posi[0])**2+(self.pos[1]-posi[1])**2) <= self.scale+size:
        return True

pygame.init()

surface = pygame.display.set_mode(SCREEN_SIZE)
font = pygame.font.SysFont(None, 24)


def main():
  global surface, font
  # initialise objects
  ship = dc(Ship(SCREEN_SIZE,pos=[SCREEN_SIZE[0]/2,SCREEN_SIZE[1]/2], scale=20))
  bullets = []
  asteroids = []
  for i in range(4):
    asteroids.append(Asteroid(SCREEN_SIZE, scale=2**random.randint(3,5)))
    while asteroids[-1].collides_with(ship):
      asteroids[-1] = Asteroid(SCREEN_SIZE, scale=2**random.randint(3,5))

  # main code
  score = 0
  bullet_cooldown = 0

  while 1:
    # time
    t = time.time()

    # update
    keys = pygame.key.get_pressed()
    if keys[pygame.K_a]:
      ship.rot -= math.radians(5)
    if keys[pygame.K_d]:
      ship.rot += math.radians(5)
    if keys[pygame.K_w]:
      ship.velocity[0] += 0.2*math.sin(ship.rot)
      ship.velocity[1] += -0.2*math.cos(ship.rot)
    if keys[pygame.K_s]:
      ship.velocity[0] += -0.2*math.sin(ship.rot)
      ship.velocity[1] += 0.2*math.cos(ship.rot)
    if keys[pygame.K_SPACE] and bullet_cooldown <= 0:
      bullet_cooldown = 10
      bullets.append(Bullet(SCREEN_SIZE, dc(ship.pos), dc(ship.rot), 5, dc(ship.velocity), 10))

    bullet_cooldown -= 1

    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        pygame.quit()
        exit()

    ship.update()

    for i,asteroid in enumerate(asteroids):
      asteroid.update()
      for bullet in bullets:
        if asteroid.collides_with(bullet):
          score += 5*asteroid.scale
          if asteroid.scale <= 8:
            asteroids.remove(asteroid)
          else:
            asteroids[i] = Asteroid(screen_size=SCREEN_SIZE, pos=asteroid.pos, scale=round(asteroid.scale/math.sqrt(2)))
            asteroids.insert(i, Asteroid(screen_size=SCREEN_SIZE, pos=asteroid.pos, scale=round(asteroid.scale/math.sqrt(2))))
          bullet.clock = 0
          break
      
      if asteroid.collides_with(ship, ship.scale*2/3):
        img = font.render("Restart - Esc", True, (255, 255, 255))
        surface.blit(img, (SCREEN_SIZE[0]/2, SCREEN_SIZE[1]/2))
        pygame.display.flip()
        return


    for bullet in bullets:
      bullet.update()
      if bullet.clock <= 0:
        bullets.remove(bullet)

    if random.random() < 1/(60*20): # average one per 50 seconds
      asteroids.append(Asteroid(SCREEN_SIZE, scale=2**random.randint(3,5)))
      while asteroids[-1].collides_with(ship):
        asteroids[-1] = Asteroid(SCREEN_SIZE, scale=2**random.randint(3,5))


    # draw
    surface.fill((0,0,0))
    ship.draw(surface)
    for asteroid in asteroids:
      asteroid.draw(surface)
    for bullet in bullets:
      bullet.draw(surface)

    img = font.render("Score " + str(score), True, (255, 255, 255))
    surface.blit(img, (20, 20))


    pygame.display.flip()

    # time
    t2 = time.time()
    if t2-t < 1/60:
      time.sleep(1/60-(t2-t))


if __name__ == "__main__":
  while 1:
    main()
    cont = True
    while cont:
      for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_ESCAPE:
            cont = False